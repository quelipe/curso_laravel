<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    protected $table  = "tb_cliente";
    protected $primaryKey = "id_cliente";
    
    const CREATED_AT = "dt_inc";
    const UPDATED_AT = "dt_alt";
    
    //public $timestamp = false;
    
    protected $fillable = [
        'nm_cliente',
        'ds_endereco',
        'fl_ativo',
    ];
}
