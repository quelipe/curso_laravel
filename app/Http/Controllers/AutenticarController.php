<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AutenticarController extends Controller
{
    //
    public function logar(Request $request) {
        
        $credenciais = [
            'ds_login' => $request->usuario,
            'password' => $request->senha
        ];
        
        if(Auth::attempt($credenciais)) {
            //dd(Auth::user());
            // redirecionar para a tela
            return redirect(route('listarClientes'));
        }
        return redirect(route('login'))->with("mensagem", "Dados inválidos");
    }
    
    public function logout() {
        Session::flush();
        Auth::logout();
        return redirect(route('login'))->with("mensagem", "Logout efetuado com sucesso");
    }
}
