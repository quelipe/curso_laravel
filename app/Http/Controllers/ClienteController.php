<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Cliente;

class ClienteController extends Controller
{
    //
    private $rules = [
        'nm_cliente' => 'required|max:80|email',
    ];
    
    private $messages = [
        'required' => 'Campos obrigatórios não informados.',
        'max' => 'Limite de caracteres.',
        'email' => 'E-mail inválido.'
    ];
    
    
    
    public function view() {
        return view('cliente.cadastro');
    }
    
    public function store(Request $request) {
        $validar = Validator::make($request->all(),$this->rules,$this->messages);
        if($validar->fails()) {
            return back()->withErrors($validar->errors())->withInput();
        }
        Cliente::create($request->all());
        return redirect(route('cadastro'))->with("mensagem", 'Cadastrado com sucesso!');
    }
    
    public function listarClientes() {
        $lista = Cliente::where('fl_ativo', 1)->get();
        return view('cliente.listar',['lista' => $lista]);
    }
    
    public function telaEditarCliente($idCliente) {
        //$cliente = Cliente::find($idCliente);
        $cliente = $this->recuperarUmCliente($idCliente);
        //$cliente = Cliente::where('fl_ativo', 1)->find($idCliente);
        return view('cliente.cadastro',['cliente' => $cliente]);
    }
    
    public function alterar(Request $request) {
        //$cliente = Cliente::where('id_cliente', $request->id_cliente)->first();
        $cliente = $this->recuperarUmCliente($request->id_cliente);
        
        $cliente->update($request->all());
        return $this->listarClientes();
    }
    
    public function visualizarCliente($idCliente) {
        //$cliente = Cliente::where('id_cliente', $idCliente)->first();
        $cliente = $this->recuperarUmCliente($idCliente);
        return view('cliente.visualizar',['cliente' => $cliente]);
    }
    
    private function recuperarUmCliente($id) {
        return Cliente::where('id_cliente', $id)->first();
    }
    
    public function excluirCliente($id) {
        $cliente = $this->recuperarUmCliente($id);
        
        if($cliente) {
            $cliente->delete();
            return redirect(route('listarClientes'))->with("mensagem", 'Excluído com sucesso!');
            //return $this->listarClientes();
        } else {
            return redirect(route('listarClientes'))->with("mensagem", 'Registro não existe!');
        }
    }
    
    public function excluirClienteLogico($id) {
        $cliente = $this->recuperarUmCliente($id);
        
        if($cliente) {
            $cliente->update(['fl_ativo' => 0]);
            return redirect(route('listarClientes'))->with("mensagem", 'Excluído com sucesso!');
            //return $this->listarClientes();
        } else {
            return redirect(route('listarClientes'))->with("mensagem", 'Registro não existe!');
        }
    }
}
