<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TesteController extends Controller
{
    //
    
    public $nome;
    public $nomeSobrenome; 
    
    public function escreverString(Request $request) {
        //dd($minhavariavel, $request->id);
        //return "Olá mundo! Seja bem vindo ".$request->nome;
        return view("teste/teste");
    }
    
    public function escreverComParametro(Request $request) {
        //dd($minhavariavel, $request->id);
        return "Nome do cliente: ".$request->nome;
        //return view("teste/parametro");
    }
    
    private function escreverDentroDaClasse() {
        return "Dentro da classe";
    }
    
    protected function escreverParaOutrasClasses() {
        return "Outras classes";
    }
    
    
}
