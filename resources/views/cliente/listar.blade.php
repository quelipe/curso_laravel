@extends('layout/template')
@section('content')
<div class="container">
    <h1>Listar cliente</h1>
    @if(session('mensagem'))
    	<div class="alert alert-danger auto-fechar text-center">
            <strong>{{session('mensagem')}}</strong>
        </div>
    @endif
    <button class="btn btn-success" onclick="document.location='/cadastro'">Cadastrar</button>
	<table class="table">
		<tr>
			<th>Código</th>
			<th>Nome</th>
			<th>Endereço</th>
			<th>Ações</th>
		</tr>
		@foreach($lista as $valor)
			<tr>
				<td>{{$valor->id_cliente}}</td>
    			<td>{{$valor->nm_cliente}}</td>
    			<td>{{$valor->ds_endereco}}</td>
    			<td>
    				<a href="telaEditarCliente/{{$valor->id_cliente}}">Editar</a>&nbsp;&nbsp;&nbsp;&nbsp;
    				<a href="visualizarCliente/{{$valor->id_cliente}}">Visualizar</a>&nbsp;&nbsp;&nbsp;&nbsp;
    				<a onclick="excluir({{$valor->id_cliente}})" href="#">Excluir</a>&nbsp;&nbsp;&nbsp;&nbsp;
    				<a onclick="excluirLogico({{$valor->id_cliente}})" href="#">Excluir Lógico</a>
    			</td>
			</tr>
		@endforeach
		
		
		<?php 
// 		foreach ($lista as $valor) {
// 		      echo "<tr>
//         		      <td>".$valor->id_cliente."</td>
//         		      <td>".$valor->nm_cliente."</td>
//         		      <td>".$valor->ds_endereco."</td>
//     		      </tr>";
// 		  }
		?>
		
	</table>
    

</div>
<script>
	function excluir(id) {
		if(confirm('Deseja realmente excluir o registro?')) {
			document.location='/excluirCliente/'+id;
		}
		return false;
	}
	
	function excluirLogico(id) {
		if(confirm('Deseja realmente excluir o registro?')) {
			document.location='/excluirClienteLogico/'+id;
		}
		return false;
	}
</script>
    
    
@endsection