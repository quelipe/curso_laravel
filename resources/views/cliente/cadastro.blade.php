@extends('layout/template')
@section('content')
<div class="container">
    <h1>{{isset($cliente->id_cliente) ? 'Alterar' : 'Novo'}} cliente</h1>
    
    <form action="{{isset($cliente->id_cliente) ? '/alterar' : '/cadastrar'}}" class="" method="post">
        @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    	@endif
    	@csrf
    	<input type="hidden" name="id_cliente" value="{{$cliente->id_cliente ?? ''}}">
    	<label>Nome do cliente</label><br>
    	<input class="form-control" type="text" value="{{$cliente->nm_cliente ?? old('nm_cliente')}}" name="nm_cliente" id="nm_cliente">   
    	<br>
    	<label>Endereço do cliente</label><br>
    	<input class="form-control" required="required" type="text" value="{{$cliente->ds_endereco ?? old('ds_endereco')}}"  name="ds_endereco" id="ds_endereco">   
    	<br>
    	<button type="submit" class="btn btn-primary">Enviar</button>
    	@if(session('mensagem'))
    	<div class="alert alert-danger auto-fechar text-center">
            <strong>{{session('mensagem')}}</strong>
        </div>
        @endif
    	
    </form>

</div>  
    
    
@endsection