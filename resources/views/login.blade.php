<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
  

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
    	<form method="post" action="/logar">
    		@csrf
        	Login: <br>
            <input type="text" name="usuario" value="" class="form-control">
            <br>Senha: <br>
            <input type="password" name="senha" value="" class="form-control">
    		<br><br>
    		<button type="submit" class="btn btn-primary">Autenticar</button>
    	</form>
    	@if(session('mensagem'))
    	<div class="alert alert-danger auto-fechar text-center">
            <strong>{{session('mensagem')}}</strong>
        </div>
        @endif
    </body>
</html>
