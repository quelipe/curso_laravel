<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\AutenticarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return view('cliente/cadastro');
// });



// Aqui houve uma alteração

Route::view('/', 'login')->name('login');
Route::post('/logar', [AutenticarController::class, 'logar'])->name('logar');
Route::get('/sair', [AutenticarController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function() {
    Route::get('/listarClientes', [ClienteController::class, 'listarClientes'])->name('listarClientes');
    Route::get('/cadastro', [ClienteController::class, 'view'])->name('cadastro');
    Route::post('/cadastrar', [ClienteController::class, 'store'])->name('cadastrar');
    Route::post('/alterar', [ClienteController::class, 'alterar'])->name('alterar');
    
    Route::get('/telaEditarCliente/{id}', [ClienteController::class, 'telaEditarCliente'])->name('telaEditarCliente');
    
    Route::get('/visualizarCliente/{id}', [ClienteController::class, 'visualizarCliente'])->name('visualizarCliente');
    
    Route::get('/excluirCliente/{id}', [ClienteController::class, 'excluirCliente'])->name('excluirCliente');
    
    Route::get('/excluirClienteLogico/{id}', [ClienteController::class, 'excluirClienteLogico'])->name('excluirClienteLogico');
});



