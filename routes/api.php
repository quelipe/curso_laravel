<?php

use App\Http\Controllers\TesteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::controller(TesteController::class)->group(function() {
    Route::group(['prefix' => 'teste'], function() {
        Route::get('/', 'escreverString');
        Route::get('/{nome}', 'escreverComParametro');
    });
        
    Route::group(['prefix' => 'clientes'], function() {
        Route::get('/', 'escreverString');
        Route::get('/{nome}', 'escreverComParametro');
    });
});